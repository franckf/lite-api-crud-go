module lite-api-crud

go 1.16

require (
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-sqlite3 v1.14.8
	golang.org/x/crypto v0.0.0-20210921155107-089bfa567519
)
